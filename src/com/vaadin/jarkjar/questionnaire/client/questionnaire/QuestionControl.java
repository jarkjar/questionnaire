package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.Question.QuestionType;

/**
 * Represents Question item in QuestionnaireWidget. Wraps all fields used in
 * question. Handle user answer returning functionality.
 * 
 * @author Jarkko Järvinen
 * 
 */
public class QuestionControl extends VerticalPanel {
    private Question question;
    private FlowPanel questionPanel;
    private FlowPanel answerPanel;

    private HTML validatorField;

    public QuestionControl(Question question) {
        this.question = question;

        this.setStyleName(QuestionnaireWidget.CLASSNAME + "-questioncontrol");

        questionPanel = new FlowPanel();
        questionPanel.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                + "-question");

        answerPanel = new FlowPanel();
        answerPanel.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                + "-answer");

        // Add indicator message
        validatorField = new HTML(question.getRequiredIndicator());
        validatorField.setStyleName(QuestionnaireWidget.CLASSNAME
                + "-requiredfield");
        validatorField.setVisible(question.isRequired());

        // Add question
        final Label questionLabel = new Label();
        questionLabel.setText(question.getText());
        questionLabel.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                + "-label");
        questionPanel.add(questionLabel);
        questionPanel.add(validatorField);

        // Set answer fields
        switch (question.getType()) {
        case TEXTFIELD:
            TextBox answerField = new TextBox();
            answerField.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                    + "-textfield");
            answerField.setMaxLength(question.getAnswerMaxLength());
            answerPanel.add(answerField);
            break;
        case TEXTAREA:
            TextArea answerArea = new TextArea();
            answerArea.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                    + "-textarea");
            answerPanel.add(answerArea);
            break;
        case CHECKBOX:
            for (String answer : question.getAnswers()) {
                CheckBox checkBox = new CheckBox(answer);
                checkBox.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                        + "-checkbox");
                checkBox.setFormValue(answer);
                answerPanel.add(checkBox);
            }
            break;
        case RADIOBUTTON:
            for (String answer : question.getAnswers()) {
                RadioButton radioButton = new RadioButton("radioGroup-"
                        + question.getId(), answer);
                radioButton.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                        + "-radiobutton");
                radioButton.setFormValue(answer);
                if (answerPanel.getWidgetCount() == 0) {
                    radioButton.setValue(true);
                }
                answerPanel.add(radioButton);
            }
            break;
        default:
            break;
        }

        add(questionPanel);
        add(answerPanel);
    }

    /**
     * Return user answer. Selected CheckBox values will be returned in comma
     * separated string presentation. Otherwise value of the answered field.
     * 
     * @return
     */
    public String getUserAnswer() {
        switch (this.question.getType()) {
        case TEXTAREA:
        case TEXTFIELD:
            TextBoxBase answerField = (TextBoxBase) answerPanel.getWidget(0);
            return answerField.getText();
        case CHECKBOX:
            StringBuilder stringBuilder = new StringBuilder();
            for (Widget widget : answerPanel) {
                CheckBox checkBox = (CheckBox) widget;
                if (checkBox.getValue()) {
                    if (stringBuilder.length() > 0) {
                        stringBuilder.append(',');
                    }
                    stringBuilder.append(checkBox.getText());
                }
            }
            return stringBuilder.toString();
        case RADIOBUTTON:
            for (Widget widget : answerPanel) {
                RadioButton radioButton = (RadioButton) widget;
                if (radioButton.getValue()) {
                    return radioButton.getText();
                }
            }
        default:
            return "";
        }
    }

    /**
     * @return the question id
     */
    public int getQuestionId() {
        return this.question.getId();
    }

    /**
     * Return information is question control valid
     * 
     * @return
     */
    public boolean isValid() {
        boolean isValid = true;
        String userAnswer = this.getUserAnswer();
        if (userAnswer.length() > 0
                && (this.question.getType() == QuestionType.TEXTFIELD || this.question
                        .getType() == QuestionType.TEXTAREA)
                && userAnswer.length() > this.question.getAnswerMaxLength()) {
            isValid = false;
            validatorField.setVisible(true);
            validatorField.setHTML(question.getMaxLengthError());
            validatorField.setStyleName(QuestionnaireWidget.CLASSNAME
                    + "-errormessage");
        } else if (this.question.isRequired() && userAnswer.equals("")) {
            isValid = false;
            validatorField.setHTML(question.getRequiredError());
            validatorField.setStyleName(QuestionnaireWidget.CLASSNAME
                    + "-errormessage");
        } else {
            validatorField.setHTML(question.getRequiredIndicator());
            validatorField.setStyleName(QuestionnaireWidget.CLASSNAME
                    + "-requiredfield");
        }
        return isValid;
    }
}
