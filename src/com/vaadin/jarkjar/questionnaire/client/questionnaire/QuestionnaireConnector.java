package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.jarkjar.questionnaire.Questionnaire;
import com.vaadin.shared.ui.Connect;

@Connect(Questionnaire.class)
public class QuestionnaireConnector extends AbstractComponentConnector
        implements ClickHandler {

    /**
     * Contains question controls for QuestionnaireWidget those will be shown in
     * form
     */
    private List<QuestionControl> questionControls;

    /**
     * Questionnaire title goes here
     */
    private Label questionnaireTitle;

    /**
     * Questionnaire description goes here
     */
    private Label questionnaireDescription;

    /**
     * Submit button for QuestionnaireWidget
     */
    final Button submitButton;

    QuestionnaireServerRpc serverRpc = RpcProxy.create(
            QuestionnaireServerRpc.class, this);

    /**
     * Default constructor
     */
    public QuestionnaireConnector() {
        questionControls = new ArrayList<QuestionControl>();

        // Initialize user controls for client
        submitButton = new Button("Submit");
        submitButton.addClickHandler(this);
        getWidget();
        submitButton.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                + "-submitbutton");

        questionnaireTitle = new Label();
        getWidget();
        questionnaireTitle.setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                + "-title");

        questionnaireDescription = new Label();
        getWidget();
        questionnaireDescription
                .setStylePrimaryName(QuestionnaireWidget.CLASSNAME
                        + "-description");
    }

    @Override
    protected Widget createWidget() {
        return GWT.create(QuestionnaireWidget.class);
    }

    @Override
    public QuestionnaireWidget getWidget() {
        return (QuestionnaireWidget) super.getWidget();
    }

    @Override
    public QuestionnaireState getState() {
        return (QuestionnaireState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        QuestionSet questionSet = getState().getQuestionSet();

        // Get questions from server
        final List<Question> questions = questionSet.getQuestions();
        // Clear exist questionControls
        questionControls.clear();
        getWidget().clear();

        // Set custom text to submitButton
        String text = questionSet.getSubmitButtonText();
        if (text != null) {
            this.submitButton.setText(text);
        }
        String title = questionSet.getText();
        if (title != null) {
            this.questionnaireTitle.setText(title);
            getWidget().add(questionnaireTitle);
        }
        String description = questionSet.getDescription();
        if (description != null) {
            this.questionnaireDescription.setText(description);
            getWidget().add(questionnaireDescription);
        }

        // Add new ones to widget
        for (Question question : questions) {
            QuestionControl qc = new QuestionControl(question);
            questionControls.add(qc);

            getWidget().add(qc);
        }

        // We need this submit button to indicate server that user has been
        // answered
        getWidget().add(submitButton);
    }

    @Override
    public void onClick(ClickEvent event) {
        boolean isValid = true;
        // Get user answers and transmit those to server
        List<UserAnswer> userAnswers = new ArrayList<UserAnswer>();
        for (QuestionControl qc : questionControls) {
            // Validate each control
            isValid &= qc.isValid();
            // Do not add any answers if validation fails
            if (isValid) {
                userAnswers.add(new UserAnswer(qc.getQuestionId(), qc
                        .getUserAnswer()));
            }
        }

        // If not valid then return
        if (!isValid) {
            return;
        }

        serverRpc.setUserAnswers(userAnswers);

        // This will cause event fire.
        serverRpc.submitButtonClicked();
    }
}
