package com.vaadin.jarkjar.questionnaire;

import java.util.List;

import com.vaadin.jarkjar.questionnaire.Questionnaire.SubmitButtonClickEvent;
import com.vaadin.jarkjar.questionnaire.Questionnaire.SubmitButtonClickListener;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.Question;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.Question.QuestionType;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.QuestionSet;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.UserAnswer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class QuestionnaireUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);

        QuestionSet questionSet = new QuestionSet();
        questionSet.setText("Testing addon");
        questionSet.setDescription("This is questionnaire testing.");
        questionSet.setId(1);
        questionSet.setSubmitButtonText("Send");

        Question q1 = new Question(1, "Enter your name");
        q1.setAnswerMaxLength(100);
        q1.setRequired(true);
        q1.setRequiredIndicator("*");
        q1.setRequiredError("Name is required");
        questionSet.add(q1);

        Question q2 = new Question(2, "Description", QuestionType.TEXTAREA);
        q2.setAnswerMaxLength(4000);
        questionSet.add(q2);

        Question q3 = new Question(3, "Interestings", QuestionType.CHECKBOX);
        q3.setRequired(true);
        q3.setRequiredIndicator("*");
        q3.setRequiredError("Interestings are required");
        q3.addAnswer("Sports");
        q3.addAnswer("Music");
        q3.addAnswer("Movies");
        q3.addAnswer("Books");
        questionSet.add(q3);

        Question q4 = new Question(4, "Gender", QuestionType.RADIOBUTTON);
        q4.addAnswer("Male");
        q4.addAnswer("Female");
        q4.setRequiredError("Smart ass!");
        questionSet.add(q4);

        Question q5 = new Question(5, "Country", QuestionType.RADIOBUTTON);
        q5.addAnswer("Finland");
        q5.addAnswer("Other");
        q5.setRequiredError("Smart ass!");
        questionSet.add(q5);

        final Questionnaire questionnaire = new Questionnaire();
        questionnaire.addClickListener(new SubmitButtonClickListener() {
            @Override
            public void buttonClick(SubmitButtonClickEvent event) {
                final List<UserAnswer> answers = questionnaire.getUserAnswers();
                StringBuilder sb = new StringBuilder();
                if (answers != null) {
                    sb.append("Save to database:");
                    for (UserAnswer answer : answers) {
                        if (sb.length() > 0) {
                            sb.append("\n");
                        }
                        sb.append(answer.getQuestionId() + ": "
                                + answer.getValue());
                    }
                } else {
                    sb.append("No data");
                }
                Notification.show(sb.toString());
            }
        });
        questionnaire.setDataSource(questionSet);
        layout.addComponent(questionnaire);
    }

}
