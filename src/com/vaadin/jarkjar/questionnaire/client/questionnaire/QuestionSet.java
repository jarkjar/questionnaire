package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Data source container for Questionnaire
 * 
 * @author Jarkko Järvinen
 * 
 */
public class QuestionSet implements Serializable {
    private int id;
    private String text;
    private String description;
    private List<Question> questions;
    private String submitButtonText;

    public QuestionSet() {
        this.questions = new ArrayList<Question>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the question
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * @param question
     *            the question to set
     */
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    /**
     * Add question to questionSet
     * 
     * @param question
     */
    public void add(Question question) {
        this.questions.add(question);
    }

    /**
     * Set submit button text
     * 
     * @param text
     */
    public void setSubmitButtonText(String text) {
        this.submitButtonText = text;
    }

    /**
     * @return the submitButtonText
     */
    public String getSubmitButtonText() {
        return this.submitButtonText;
    }

}
