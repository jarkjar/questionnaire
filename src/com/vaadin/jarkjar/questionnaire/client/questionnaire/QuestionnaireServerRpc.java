package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import java.util.List;

import com.vaadin.shared.communication.ServerRpc;

public interface QuestionnaireServerRpc extends ServerRpc {
    public void submitButtonClicked();

    public void setUserAnswers(List<UserAnswer> userAnswers);
}
