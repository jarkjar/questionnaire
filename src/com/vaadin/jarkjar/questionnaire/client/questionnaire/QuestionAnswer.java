package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import java.io.Serializable;

public class QuestionAnswer implements Serializable {
    private int id;
    private String value;

    // private boolean selected;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the selected
     */
    // public boolean isSelected() {
    // return selected;
    // }

    /**
     * @param selected
     *            the selected to set
     */
    // public void setSelected(boolean selected) {
    // this.selected = selected;
    // }
}
