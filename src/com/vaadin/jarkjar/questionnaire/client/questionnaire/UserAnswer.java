package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import java.io.Serializable;

public class UserAnswer implements Serializable {
    private int questionId;
    private String value;

    public UserAnswer() {
    }

    public UserAnswer(int questionId, String value) {
        this.questionId = questionId;
        this.value = value;
    }

    /**
     * @return the questionId
     */
    public int getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId
     *            the questionId to set
     */
    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
